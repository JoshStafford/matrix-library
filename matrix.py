
class Matrix:


	def __init__(self, rows, cols, data=None):

		self.rows = rows
		self.cols = cols
		self.data = []

		if(data is not None):

			self.data = data

		else:

			for i in range(self.rows):

				self.data.append([])

				for j in range(self.cols):

					self.data[i].append(0)


	def product(self, val):

		for i in range(self.rows):

			for j in range(self.cols):

				self.data[i][j] *= val

	@staticmethod
	def mat_add(mat1, mat2):

		if(mat1.rows != mat2.rows or mat1.cols != mat2.cols):
			return None

		result = []
		for i in range(mat1.rows):
			result.append([])
			for j in range(mat1.cols):
				result[i].append(0)
				result[i][j] = mat1.data[i][j] + mat2.data[i][j]

		return Matrix(mat1.rows, mat1.cols, result)

	def add(self, val):

		for i in range(self.rows):

			for j in range(self.cols):

				self.data[i][j] += val

	
	def transpose(self):

		result = []

		for i in range(self.cols):

			result.append([])			

			for j in range(self.rows):

				result[i].append(0)



		for i in range(self.cols):

			for j in range(self.rows):

				result[i][j] = self.data[j][i]

		self.data = result

		self.rows, self.cols = self.cols, self.rows


	def show(self):

		for i in range(self.rows):

			print(self.data[i])

	@staticmethod
	def dimensions(mat):

		rows = len(mat.data)
		cols = len(mat.data[0])

		return (rows, cols)


dat =  [[0, 0, 2, 0, 0, 0],
		[0, 1, 0, 1, 0, 0],
		[0, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 1, 0],
		[4, 0, 0, 0, 0, 0],
		[0, 0, 0, 0, 0, 2]]

dat1 =  [[1, 0, 3, 0, 0, 0],
		[0, 1, 0, 1, 0, 0],
		[0, 0, 0, 0, 0, 0],
		[0, 0, 0, 2, 1, 0],
		[4, 0, 1, 0, 0, 0],
		[0, 0, 1, 0, 0, 2]]	

adj =  [[1, 0, 0],
		[0, 1, 0],
		[0, 0, 1]]


class Sparse_Matrix:


	def __init__(self, rows, cols, data=None):

		self.rows = rows
		self.cols = cols
		self.data = []

		if(data is not None):

			for i in range(self.rows):

				for j in range(self.cols):

					if(data[i][j] != 0):

						self.data.append({
							"pos" : (i, j),
							"val" : data[i][j]
						})

	
	def gen_matrix(self):

		result = []

		for i in range(self.rows):

			result.append([])

			for j in range(self.cols):

				result[i].append(0)

		
		for value in self.data:

			result[value["pos"][0]][value["pos"][1]] = value["val"]

		return result

	@staticmethod
	def gen_empty_matrix(rows, cols):

		result = []

		for i in range(rows):

			result.append([])

			for j in range(cols):

				result[i].append(0)

		return result


	def dimensions(self):

		return (self.rows, self.cols)

	
	@staticmethod
	def sparse_sum(mat1, mat2):

		result = mat1.gen_matrix()

		for value in mat2.data:

			result[value["pos"][0]][value["pos"][1]] += value["val"]

		return Sparse_Matrix(mat1.rows, mat1.cols, result)

	def transpose(self):

		new_data = []

		for value in self.data:

			new_data.append({
				"pos" : (value["pos"][1], value["pos"][0]),
				"val" : value["val"]
			})

		self.data = new_data
		self.rows, self.cols = self.cols, self.rows

	@staticmethod
	def show(mat):

		for row in mat:
			print(row)


	# --> FINISH MATMULT FUNCTION, NOT WORKING <--
	@staticmethod
	def matmult(matA, matB):

		result = Sparse_Matrix.gen_empty_matrix(matA.rows, matB.cols)

		for i in range(len(result)):

			for j in range(len(result[0])):

				val = 0

				for k in range(len(result[0])):


					for value1 in matA.data:


							for value2 in matB.data:

								if (value1["pos"] == (i, k) and value2["pos"] == (k, i)):

									val += value1["val"] * value2["val"]

				result[i][j] = val

		return Sparse_Matrix(matA.rows, matB.cols, result)

					
class Adj_Matrix(Sparse_Matrix):

	def isfunction(self):

		for i in range(self.cols):

			count = 0

			for value in self.data:

				if(value["pos"][1] == i):

					count += 1

			if count > 1:
				return False

		return True

	
	def istotal(self):

		for i in range(self.cols):

			count = 0

			for value in self.data:

				if(value["pos"][1] == i):

					count += 1

			if count < 1:
				return False

		return True
		
m = Adj_Matrix(3, 3, adj)